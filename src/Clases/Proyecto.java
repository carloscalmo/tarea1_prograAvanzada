package Clases;

/**
 * Created by carlos on 07/07/2017.
 */
public class Proyecto {
    private String nombre;
    private String propietario;
    private String ubicacion;
    private float costo;

    public Proyecto() {
    }

    public String getNombre() {
        return nombre;
    }

    public String getPropietario() {
        return propietario;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public float getCosto() {
        return costo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }
}
