package Principal;

import Clases.Insumo;
import Clases.Propietario;
import Clases.Proyecto;
import Clases.Renglon;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleApp {
    public static void main(String[] args) throws Exception{
        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir){
            System.out.println("Bienvenido.\nIngrese la opción que desea ejecutar:\n\n");
            System.out.println("1. Ingresar un nuevo cliente o propietario.\n" +
                                "2. Ingresar un nuevo proyecto.\n" +
                                "3. Ingresar un nuevo renglón.\n" +
                                "4. Ingresar un nuevo insumo\n" +
                                "9. Salir");
            opcion = sn.nextInt();
            sn.skip("\n");
            switch(opcion){

                //PROPIETARIO
                case 1:
                    Propietario propietario = new Propietario();
                    System.out.println("Ingrese los datos del nuevo cliente:\n" +
                    "Nombre:");
                    propietario.setNombre(sn.nextLine());
                    System.out.println("Dirección:");
                    propietario.setDireccion(sn.nextLine());
                    System.out.println("Municipio:");
                    propietario.setMunicipio(sn.nextLine());
                    System.out.println("Departamento:");
                    propietario.setDepartamento(sn.nextLine());
                    System.out.println("Nit:");
                    propietario.setNit(sn.nextInt());
                    sn.skip("\n");
                    System.out.println("Teléfono:");
                    propietario.setTelefono(sn.nextInt());
                    sn.skip("\n");
                break;

                //PROYECTO
                case 2:
                    Proyecto proyecto = new Proyecto();
                    System.out.println("Ingrese los datos del nuevo proyecto:\n" +
                    "Nombre:");
                    proyecto.setNombre(sn.nextLine());
                    System.out.println("Propietario:");
                    proyecto.setPropietario(sn.nextLine());
                    System.out.println("Ubicación:");
                    proyecto.setUbicacion(sn.nextLine());
                    System.out.println("Costo total del proyecto:");
                    proyecto.setCosto(sn.nextFloat());
                break;

                //RENGLÓN
                case 3:
                    Renglon renglon = new Renglon();
                    System.out.println("Ingrese los datos del nuevo renglón de trabajo:\n" +
                    "Nombre:");
                    renglon.setNombre(sn.nextLine());
                    System.out.println("Unidad:");
                    renglon.setUnidad(sn.next());

                    //añadir insumos a la lista del renglón
                    boolean bandera = true;
                    List<Insumo> insumosList = new ArrayList<Insumo>();
                    while(bandera){
                        //llenar datos del insumo
                        int indicador =0;
                        Insumo insumoRenglon = new Insumo();
                        System.out.println("Ingrese los datos de los insumos que componen el renglón de trabajo:\n" +
                        "Descripción:");
                        insumoRenglon.setDescripcion(sn.next());
                        System.out.println("Unidad:");
                        insumoRenglon.setUnidad(sn.next());
                        System.out.println("Cantidad:");
                        insumoRenglon.setCantidad(sn.nextFloat());
                        System.out.println("Precio unitario:");
                        insumoRenglon.setPrecioUnitario(sn.nextFloat());
                        Float total = insumoRenglon.calculatePrecioTotal();
                        System.out.println("Precio total: "+total);

                        //agregar insumo a la lista
                        insumosList.add(insumoRenglon);


                        //agregar uno más
                        System.out.println("¿Desea agregar un insumo más a este renglón? " +
                        "\n 1. Si \n 2. No");
                        indicador = sn.nextInt();
                        switch(indicador){
                            case 1:
                                bandera = true;
                            break;
                            case 2:
                                bandera = false;
                            break;
                        }
                    }

                    //calcular el total del renglón
                    float totalRenglon=0;
                    for (int i = 0; i <= insumosList.size()-1; i++){
                        totalRenglon = insumosList.get(i).calculatePrecioTotal()+totalRenglon;
                    }
                    renglon.setPrecioUnitario(totalRenglon);
                    System.out.println("Precio unitario del renglón: "+totalRenglon);
                break;

                //INSUMO
                case 4:
                    Insumo insumo = new Insumo();
                    System.out.println("Ingrese los datos del nuevo insumo:\n" +
                    "Descripción:");
                    insumo.setDescripcion(sn.next());
                    System.out.println("Unidad:");
                    insumo.setUnidad(sn.next());
                    System.out.println("Cantidad:");
                    insumo.setCantidad(sn.nextFloat());
                    System.out.println("Precio unitario:");
                    insumo.setPrecioUnitario(sn.nextFloat());
                    Float total = insumo.calculatePrecioTotal();
                    System.out.println("Precio total: "+total);


                    break;

                case 9:
                    System.exit(1);
                break;
            }
        }
    }
}
